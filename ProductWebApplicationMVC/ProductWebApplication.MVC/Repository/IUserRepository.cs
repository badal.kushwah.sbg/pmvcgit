﻿using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Repository
{
    public interface IUserRepository
    {
        User? GetUserByName(string? name);
        bool AddUser(User user);
        List<User> GetAllUsers();
        User Login(string? name, string? password);
    }
}
