﻿using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Services
{
    public interface IUserService
    {
        bool AddUser(User user);
        List<User> GetAllUsers();
        User Login(LoginUser loginUser);
    }
}

